package main

import "net/http"

func controllerReadiness(w http.ResponseWriter, request *http.Request) {
	responseWithJSON(w, 200, map[string]interface{}{
		"status": 200,
	})
}
