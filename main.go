package main

import (
	"fmt"
	"log"
	"os"

	"github.com/joho/godotenv"

	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/cors"
)

func main() {
	// check you can load .env file
	dotEnvErr := godotenv.Load()
	if dotEnvErr != nil {
		log.Fatal("Error loading .env file")
	}
	port := os.Getenv("PORT")
	if port == "" {
		log.Fatal("PORT not found in the environment")
	}
	router := chi.NewRouter()
	router.Use(cors.AllowAll().Handler)

	v1Router := chi.NewRouter()
	v1Router.Get("/healthz", controllerReadiness)
	v1Router.Get("/err", controllerErr)
	router.Mount("/v1", v1Router)

	fmt.Printf("Server running on port %v", port)

	serverErr := http.ListenAndServe(":"+port, router)
	if serverErr != nil {
		log.Fatal(serverErr)
	}
}
