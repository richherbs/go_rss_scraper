package main

import "net/http"

func controllerErr(w http.ResponseWriter, request *http.Request) {
	responseWithError(w, 500, "Internal Server Error")
}
