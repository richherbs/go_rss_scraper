package main

import (
	"encoding/json"
	"log"
	"net/http"
)

func responseWithJSON(w http.ResponseWriter, status int, payload interface{}) {

	data, jsonErr := json.Marshal(payload)

	if jsonErr != nil {
		log.Printf("Failed to marshal JSON: %v", jsonErr)
		w.WriteHeader(500)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	w.Write(data)
}

func responseWithError(w http.ResponseWriter, code int, msg string) {

	if code > 499 {
		log.Printf("Responding with 500 error: %v", msg)
	}

	type errResponse struct {
		Error string `json:"error"`
	}

	w.Header().Set("Content-Type", "text/plain")
	w.WriteHeader(code)

	responseWithJSON(w, code, errResponse{
		Error: msg,
	})
}
